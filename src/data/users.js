const users = [
  { id: 'u01', name: 'Alex', age: 32 },
  { id: 'u02', name: 'Andrew', age: 28 },
];

export default users;