import React from 'react';

import styles from './Card.module.css';

export default function Card({ children, className }) {
  const classList = styles.card + (className ? ` ${className}` : '');
  
  return (
    <div className={classList}>
      {children}
    </div>
  );
};