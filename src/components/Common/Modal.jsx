import React from 'react';

import styles from './Modal.module.css';

export default function Modal({
  header,
  message,
  visible,
  onHide,
}) {
  const classList = styles.modal + (visible ? ` ${styles.visible}` : '');

  return (
    <div
      className={classList}
      onClick={onHide}
    >
      <div className={styles['modal-body']}>
        <h2>{header}</h2>
        <p>{message}</p>
        <button type='button' onClick={onHide}>Okay</button>
      </div>
    </div>
  );
}