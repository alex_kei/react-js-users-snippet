import React, { useState } from 'react';
import logo from './logo.svg';
import UserControl from '../User/UserControl';
import UserList from '../User/UserList';

import styles from './App.module.css';

import defaultUsers from '../../data/users';

export default function App() {
  const [ users, setUsers ] = useState(defaultUsers);
  const handleUserSubmit = (userData) => {
    const user = {
      ...userData,
      id: Math.random(),
    };
    console.log('New User:', user);
    setUsers(currentUsers => [user, ...currentUsers]);
  } ;

  return (
    <div className={styles.App}>
      <header className={styles['App-header']}>
        <img src={logo} className={styles['App-logo']} alt="logo" />
      </header>
      <UserControl onUserSubmit={handleUserSubmit} />
      <UserList users={users} />
    </div>
  );
};
