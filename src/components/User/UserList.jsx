import React from 'react';
import Card from '../Common/Card';
import UserItem from './UserItem';

export default function UserList({ users }) {
  return (
    <Card>
      {users.map(
        ({ id, ...itemProps }) => <UserItem key={id} {...itemProps} />
      )}
    </Card>
  );
};