import React, { useState } from 'react';
import Card from '../Common/Card';

import styles from './UserForm.module.css';

export default function UserForm({ onUserSubmit, onUserError }) {
  const [ name, setName ] = useState('');
  const [ age, setAge ] = useState('');

  const resetFormData = () => {
    setName('');
    setAge('');
  };

  const validate = (data) => {
    const name = data.name.trim();
    const age = data.age.trim();
    if (name.length === 0 || age.length === 0) {
      throw new Error('Please enter a valid name and age (non-empty values).');
    }
    if (parseInt(age) <= 0) {
      throw new Error('Please enter a valid age (should be positive).');
    }
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    try {
      validate({ name, age });
      onUserSubmit({ name, age });
      resetFormData();
    } catch (error) {
      onUserError(error);
    }
  }

  return (
    <Card className={styles['user-form']}>
      <form onSubmit={handleSubmit}>
        <div>
          <label htmlFor='username'>Username:</label>
          <input
            id='username'
            type='text'
            value={name}
            onChange={({ target }) => setName(target.value)}
          />
        </div>
        <div>
          <label htmlFor='age'>Age (years):</label>
          <input
            id='age'
            type='number'
            value={age}
            onChange={({ target }) => setAge(target.value)}
          />
        </div>
        <button type='submit'>Add User</button>
      </form>
    </Card>
  );
};
