import React from 'react';

import styles from './UserItem.module.css';

export default function UserItem({ name, age }) {
  return (
    <p className={styles['user-item']}>{name} ({age} years old)</p>
  );
};