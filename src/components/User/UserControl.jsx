import React, { useState } from 'react';
import Modal from '../Common/Modal';
import UserForm from './UserForm';

export default function UserControl({ onUserSubmit }) {
  const [ modalMessage, setModalMessage ] = useState('');
  const [ isModalVisible, setIsModalVisible ] = useState(false);
  const handleError = (error) => {
    console.error(error.message);
    setModalMessage(error.message);
    setIsModalVisible(true);
  };

  return (
    <>
      <UserForm onUserSubmit={onUserSubmit} onUserError={handleError} />
      <Modal
        visible={isModalVisible}
        header='Invalid Input'
        message={modalMessage}
        onHide={() => setIsModalVisible(false)}
      />
    </>
  );
};